# !pip install --upgrade pip
# !pip install pandas
# !pip install pandas-datareader
# !pip install matplotlib
# !pip install beautifulsoup4
# !pip install scikit-learn
# !pip install lxml 
# !pip install fix_yahoo_finance --upgrade --no-cache-dir
# matplotlib.mpl_finance installed without !pip if you're using yahoo finance

from matplotlib import style
import fix_yahoo_finance as yf 
import mpl_finance
from mpl_finance import candlestick_ohlc
import pandas as pd 
import pandas_datareader as web
from pandas_datareader import data
import bs4 as bs 
import pickle 
import requests 
import os 
import numpy as np
from collections import Counter
from sklearn import svm, neighbors
from sklearn.model_selection import train_test_split
from sklearn.ensemble import VotingClassifier, RandomForestClassifier

def save_sp500_tickers():
    resp = requests.get('https://en.wikipedia.org/wiki/List_of_S%26P_500_companies')
    soup = bs.BeautifulSoup(resp.text, "lxml")
    table = soup.find('table', {'class':'wikitable sortable'})
    tickers = []
    for row in table.findAll('tr')[1:]:
        ticker = row.findAll('td')[0].text
        mapping = str.maketrans(".","-")
        ticker = ticker.translate(mapping)
        if ticker == 'CBRE':
            ticker = 'NaN'
        tickers.append(ticker)
    tickers.remove('NaN')
    with open('sp500tickers.pickle','wb') as f:
        pickle.dump(tickers, f)
    print(tickers)
    return tickers

yf.pdr_override()

def get_data_from_yahoo(reload_sp500=False):
    if reload_sp500:
        tickers = save_sp500_tickers() 
    else:
        with open('sp500tickers.pickle', 'rb') as f: 
            tickers = pickle.load(f)
    if not os.path.exists('stock_dfs'):
        os.makedirs('stock_dfs')
    start = dt.datetime(2000,1,1)
    end = dt.datetime(2017,12,31)
    for ticker in tickers[:]: 
        if not os.path.exists('stock_dfs/{}.csv'.format(ticker)):
            df = data.get_data_yahoo(ticker, start, end)
            df.to_csv('stock_dfs/{}.csv'.format(ticker))
        else:
            print('Already have {}'.format(ticker)
            
get_data_from_yahoo() # we have stored 504 stocks after 10min...
                      # notice we dropped 'CBRE' because of yahoo API

def compile_data():
    with open('sp500tickers.pickle', 'rb') as f:
        tickers = pickle.load(f)
    main_df = pd.DataFrame() 
    for count, ticker in enumerate(tickers):
        df = pd.read_csv('stock_dfs/{}.csv'.format(ticker)) 
        df.set_index('Date', inplace=True) 
        df.rename(columns = {'Adj Close' : ticker}, inplace=True) 
        df.drop(['Open','High','Low','Close','Volume'], 1, inplace=True) 
        if main_df.empty:
            main_df = df 
        else:
            main_df = main_df.join(df, how = 'outer') 
        if count % 10 == 0: 
            print(count) 
    print(main_df.head())
    main_df.to_csv('sp500_joined_closes.csv')
    
compile_data() # now you created a joined file with all 'Adj Close'

style.use('ggplot')

def visualize_data():
    df = pd.read_csv('sp500_joined_closes.csv')
    df.set_index('Date', inplace=True)
    df_corr = df.corr() 
    print(df_corr.head())
    data = df_corr.values 
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    heatmap = ax.pcolor(data, cmap=plt.cm.RdYlGn) 
    fig.colorbar(heatmap)
    ax.set_xticks(np.arange(data.shape[1])+0.5, minor = False)
    ax.set_yticks(np.arange(data.shape[0])+0.5, minor = False) 
    ax.invert_yaxis()   
    ax.xaxis.tick_top() 
    column_labels = df_corr.columns 
    row_labels = df_corr.index 
    ax.set_xticklabels(column_labels)
    ax.set_yticklabels(row_labels)
    plt.xticks(rotation = 90) 
    heatmap.set_clim(-1,1) 
    plt.tight_layout()
    plt.savefig('correlations.png', dpi=(300)) 
    plt.show()
    
visualize_data() # it prints the corr table plus the graph (non-interactive)

def process_data_for_labels(ticker): 
    hm_days = 7 
    df = pd.read_csv('sp500_joined_closes.csv', index_col=0) 
    tickers = df.columns.values.tolist() 
    df.fillna(0, inplace=True) 
    for i in range(1, hm_days+1):
        df['{}_{}d'.format(ticker,i)]=(df[ticker].shift(-i)-df[ticker])/df[ticker] # IN PERCENTAGE CHANGE! new-old/ld
    df.fillna(0, inplace=True)
    return tickers, df

def buy_sell_hold(*args): # we are mapping this function to a pandas df
    cols = [c for c in args] 
    requirement = 0.04
    for col in cols:
        if col > requirement:
            return 1
        if col < -requirement:
            return -1
    return 0

def extract_featuresets(ticker):
    tickers, df = process_data_for_labels(ticker)
    df['{}_target'.format(ticker)] = list(map( buy_sell_hold,
                                               df['{}_1d'.format(ticker)],
                                               df['{}_2d'.format(ticker)],
                                               df['{}_3d'.format(ticker)],
                                               df['{}_4d'.format(ticker)],
                                               df['{}_5d'.format(ticker)],
                                               df['{}_6d'.format(ticker)],
                                               df['{}_7d'.format(ticker)]
                                             ))
    
    vals = df['{}_target'.format(ticker)].values.tolist() # we create a list of the values(0,1,-1)
    str_vals = [str(i) for i in vals]
    print('Data spread:', Counter(str_vals))
    df.fillna(0, inplace=True)
    df = df.replace([np.inf, -np.inf], np.nan) # it doesn't have inplace so we have to redefine df=
    df.dropna(inplace=True)
    df_vals = df[[ticker for ticker in tickers]].pct_change()
    df_vals = df_vals.replace([np.inf, -np.inf], np.nan)
    df_vals.fillna(0, inplace=True)
    X = df_vals.values # capital X is featuresets(daily%for all companies)
    y = df['{}_target'.format(ticker)].values # lowercase y is label set
    return X, y, df
    
extract_featuresets('XOM') # you created a per-company-basis column vs the market in which is suggested whether
                           # in that day you should buy or sell the stock

Data spread: Counter({'0': 3144, '1': 710, '-1': 674})

def do_ml(ticker):
    X, y, df = extract_featuresets(ticker)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25)
    clf = VotingClassifier([('slvc', svm.LinearSVC()), # linear vector support classifier
                            ('knn', neighbors.KNeighborsClassifier()), # knearneighbor
                            ('rfor', RandomForestClassifier())]) # random classifier
    clf.fit(X_train, y_train)
    confidence = clf.score(X_test, y_test)  
    print('accuracy:', confidence)
    predictions = clf.predict(X_test) 
    print('Predicted spread:', Counter(predictions))
    return confidence

do_ml('BAC') # this will tells you the actual spread of the 'ts_target', the predicted spread and the accuracy.

Data spread: Counter({'0': 2301, '1': 1168, '-1': 1059})
accuracy: 0.5176678445229682
Predicted spread: Counter({0: 862, -1: 205, 1: 65})